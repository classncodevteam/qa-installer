#!/bin/bash

which_platform() {

i=0

PS3='Please enter the number of your choice: '
options=("web" "mobile" exit)
select opt in "${options[@]}"
do
	i=$(($i + 1))
	if [ $i -eq  3 ] ; then return -1 && exit  ; fi
	 
	case "$REPLY" in
		1|"web")
			choix="web"
			echo "you chose web" >&2
		    	echo "You're running this script to install "$choix" automation" >&2
			
			break
		    	;;
		2|"mobile")
			choix="mobile"
		    	echo "you chose mobile" >&2
			echo "You're running this script to install "$choix" automation" >&2
			break
		    	;;
		
		3)
			exit
		    	break
		    	;;
		*)
		echo "invalid option $REPLY" >&2
	   	;;
	    esac
done
echo $choix

	
}

generate_logs(){

mkdir -p $PWD/Logs
timestamp=$(date +%Y%m%d_%H%M%S)
log="Logs/log$timestamp"
echo $log
}

user_cred() {
echo "Type Your Bitbucket Username:"
read USERNAME
echo "Type Your Bitbucket Password:"
read PWD
}


which_platformui() {

	OUTPUT= zenity --entry --title="Platform To Choose" --text="Please Choose a Platform" web mobile 

	accepted=$?
	if ((accepted != 0)); then
	    echo "something went wrong"
	    exit 1
	fi

	echo $OUTPUT

}

user_credui() {
 
    ENTRY=`zenity --password --username --title="Enter Your Bitbucket Credentials"`

    case $? in
        0)
            echo "User Name: `echo $ENTRY | cut -d'|' -f1`"
            echo "Password : `echo $ENTRY | cut -d'|' -f2`"
	    VAR_USERNAME=`echo $ENTRY | cut -d'|' -f1`
            VAR_USERPWD=`echo $ENTRY | cut -d'|' -f2`
           ;;
        1)
            echo "Stop login.";;
        -1)
            echo "An unexpected error has occurred.";;
    esac
}




