#!/bin/bash

check_robotframework3_1_2()
{
	version=$(pip3 show robotframework  | grep 3.1.2)
	if [[ $version == *"3.1.2"* ]] ; then

		echo "Robot framework 3.1.2  is installed"
	else

		echo "Robot framework 3.1.2  is not installed"
		echo "Begin installation of robot framework 3.1.2"
		install_robotframework
	fi
}

install_robotframework()
{
  	pip3 install robotframework==3.1.2 2> /dev/null
	if [ $? -eq 0 ]; then
   		echo "robotframework 3.1.2 is well installed"
	else 
   		echo "problem in the installation" && exit
	fi
}

install_robotframeworkdependency()
{
  	echo "Begin installation of $1 dependencies"
  	#cat Dependencies/requirements-$1.txt | xargs pip3 install
	pip3 install -r Dependencies/requirements-$1.txt 2> /dev/null
	if [ $? -eq 0 ]; then
   		echo "robotframework dependencies are well installed"
	else 
   		echo "robotframework dependencies are not installed" && exit
	fi
}

Customize_red()
{
	Python_path=$(python3 -m site --user-site)
	OriginalString="$(cat Dependencies/red-$1.txt)"

	NewString="$(echo ${OriginalString//MYPYTHONPATH/$Python_path})"
	CurrentPath="$(echo /home/`whoami`/repos/qa-$1)"

	echo "${NewString/MYPROJECTPATH/$CurrentPath}" > $CurrentPath/red.xml
}

clone_project()
{
	ls /home/`whoami`/repos/qa-$1/Tests 2> /dev/null
	if [ $? -eq 0 ]; then
    		echo "project already exist"
 	else 
   		git clone git@bitbucket.org:classncodevteam/qa-$1.git /home/`whoami`/repos/qa-$1/
 	fi
	Customize_red $1
}
