#!/bin/bash

check_java_jdk8()
{
	version=$(java -version 2>&1 >/dev/null  | grep openjdk)
	if [[ $version == *"openjdk"* ]] ; then
		echo "Java installed."
		echo " the java version is $version"
	else
	  	echo "Java NOT installed!"
	  	sudo apt install -y openjdk-8-jdk
	    	if [ $? -eq 0 ] ; then 
			echo "JDK install in progress ..."	
			echo "jdk-8 installed."
	    	else 
			echo "Exit, because of issue with jdk installation"	
			exit 
	    	fi
	fi
}

check_python3()
{
	python3 -V 2>&1 >/dev/null 
	if [ $? -ne 0 ] ; then
		echo "python is not installed"
	    
	else
		echo "python is installed"
		ver=$(python3 -V 2>&1 | sed 's/.* \([0-9]\).\([0-9]\).*/\1\2/')
		i=0.1
		t=$(echo $ver*$i | bc)
		echo  "python version is  $t "
	fi
}

check_pip3()
{
	pip3 -V 2>&1 >/dev/null
       if [ $? -ne 0 ] ; then
               echo "pip3 is not installed"
               echo " begin install of pip3"
               sudo apt-get -y install python3-pip
       else
               echo "pip3 is installed"
               ver=$(pip3 -V 2>&1 | grep "pip")
               echo  "pip3  version is  $ver "
       fi

}

configure_ssh()
{
	ssh -V 2>&1 >/dev/null 
	if [ $? -ne 0 ] ; then
		echo "SSH is not installed"
	    
	else
		ssh-keygen -q -t rsa -N '' -f ~/.ssh/id_rsa 2>/dev/null <<< y >/dev/null
		ssh-add ~/.ssh/id_rsa >/dev/null
		user_sshkey=$(cat ~/.ssh/id_rsa.pub)
		url="https://api.bitbucket.org/2.0/users/$1/ssh-keys"
		curl -u $1:$2 -X POST -H "Content-Type: application/json" -d "{\"key\":\"$user_sshkey\"}" $url >> $3

	fi		
}
